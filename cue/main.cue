package main

import (
    "gitlab.com/andrasi.istvan/cue:vps"
    "gitlab.com/andrasi.istvan/cue:docker"
)

vps

stacks: traefik: {
  version: string | *"2.9"

  host: string | *"traefik.\(domain)"
  auth: {
    name: string | *"traefik_auth"
    user: string
    pass: string
  }

  docker_sock: string | *"/var/run/docker.sock"

  ["acme"]: {
    server: string | *"https://acme-staging-v02.api.letsencrypt.org/directory"
    email: string
  }

  config: docker.#config & {
    name: _ | *"traefik_config"
    yaml: {}
  }

  volume: docker.#volume & {
    name: "traefik"
    target: _ | *"/var/traefik"
  }

  stackfile: {
    let config_traefik = "traefik.yml"
    configs: (config_traefik): {
      name: traefik.config.name
      external: true
    }
    let secret_traefik_auth = "traefik_auth"
    secrets: (secret_traefik_auth): {
      name: traefik.auth.name
      external: true
    }
    let volume_traefik_ssl = "traefik_ssl"
    volumes: (volume_traefik_ssl): {
      name: traefik.volume.name
      external: true
    }
    services: "main": {
      image: "traefik:" + traefik.version
      ports: [{published: 80, target: 80, mode: "host"}, {published: 443, target: 443, mode: "host"}]
      entrypoint: ["traefik"]
      command: [
        // "--log.level=DEBUG",
        "--accesslog=true",

        "--api=true",
        "--api.debug=false",
        "--api.dashboard=true",

        "--ping.manualRouting=true",

        "--entryPoints.web.address=:80",
        "--entryPoints.web.http.tls=false",
        "--entryPoints.web.http.redirections.entryPoint.to=:443",
        "--entryPoints.web.http.redirections.entryPoint.scheme=https",
        "--entryPoints.web.http.redirections.entryPoint.permanent=false",
        "--entryPoints.web.http.redirections.entryPoint.priority=999999",

        "--entryPoints.tls.address=:443",
        "--entryPoints.tls.http.tls=true",
        // "--entryPoints.tls.asdefault=true", TODO enable when feature is released (fix is already in master)
        "--entryPoints.tls.transport.lifecycle.graceTimeout=7s",
        "--entryPoints.tls.transport.lifecycle.requestAcceptGraceTimeout=7s",
        "--entryPoints.tls.transport.respondingTimeouts.idleTimeout=30m",
        "--entryPoints.tls.transport.respondingTimeouts.readTimeout=30s",
        "--entryPoints.tls.transport.respondingTimeouts.writeTimeout=30s",

        "--providers.providersThrottleDuration=3s",
        "--providers.docker.endpoint=unix:///var/run/docker.sock",
        "--providers.docker.swarmMode=true",
        "--providers.docker.network=traefik",
        "--providers.docker.exposedByDefault=false",
        "--providers.file.filename=/opt/" + config_traefik,
        "--providers.file.watch=false",
      ] + _args_acme

      _args_acme: [...=~"^--.+="] | *[]
      if traefik.acme != _|_ {
        _args_acme: [
            "--entryPoints.tls.http.tls.certResolver=letsencrypt",
            "--certificatesresolvers.letsencrypt.acme.caserver=\(traefik.acme.server)",
            "--certificatesresolvers.letsencrypt.acme.storage=\(traefik.volume.target)/letsencrypt-acme.json",
            "--certificatesresolvers.letsencrypt.acme.email=\(traefik.acme.email)",
            "--certificatesresolvers.letsencrypt.acme.tlschallenge=true",
        ]
      }

      volumes: [traefik.docker_sock + ":/var/run/docker.sock:ro", volume_traefik_ssl + ":" + traefik.volume.target]
      configs: [{source: config_traefik, target: "/opt/" + config_traefik}]
      secrets: [secret_traefik_auth]
      deploy: {
        mode: "global"
        placement: constraints: ["node.role==manager"]
      }
      #traefik: true
      #labels: [
        "traefik.http.routers.ping.rule=Host(`\(traefik.host)`) && Path(`/ping`)",
        "traefik.http.routers.ping.service=ping@internal",
        "traefik.http.services.ping.loadbalancer.server.port=0",

        "traefik.http.routers.api.rule=Host(`\(traefik.host)`)",
        "traefik.http.routers.api.service=api@internal",
        "traefik.http.routers.api.middlewares=api-auth",
        "traefik.http.middlewares.api-auth.basicauth.usersFile=/run/secrets/" + secret_traefik_auth,
        "traefik.http.services.api.loadbalancer.server.port=0",
      ]
    }
  }
}

stacks: whoami: {
  stackfile: {
    services: "server": {
      image: "traefik/whoami"
      #traefik: true
      #traefik_rule: "Host(`whoami.\(domain)`)"
    }
  }
}

stacks: plantuml: {
  version: string | *"latest"
  stackfile: {
    services: "server": {
      image: "plantuml/plantuml-server:" + plantuml.version
      #traefik: true
      #traefik_rule: "Host(`plantuml.\(domain)`)"
      #traefik_port: 8080
    }
  }
}

stacks: pypi: {
  version: string | *"latest"
  volume: name: string | *"pypi_packages"
  stackfile: {
    let volume_packages = "packages"
    volumes: (volume_packages): pypi.volume
    services: "server": {
      image: "pypiserver/pypiserver:" + pypi.version
      volumes: [volume_packages + ":/data/packages"]
      #traefik: true
      #traefik_rule: "Host(`pypi.\(domain)`)"
      #traefik_port: 8080
    }
  }
}

stacks: registry: {
  version: string | *"latest"

  host: string | *"docker.\(domain)"
  auth: {
    name: string | *"registry_auth"
    user: string
    pass: string
  }

  volume: name: string | *"registry_data"

  stackfile: {
    let volume_data = "data"
    volumes: (volume_data): registry.volume
    let secret_auth = "auth"
    secrets: (secret_auth): {
      name: auth.name
      external: true
    }
    services: "server": {
      #traefik: true
      #traefik_rule: "Host(`\(host)`)"
      #traefik_port: 5000

      image: "registry:" + registry.version
      volumes: [volume_data + ":/var/lib/registry"]
      secrets: [secret_auth]
      environment: {
        REGISTRY_AUTH_HTPASSWD_REALM: "Docker Registry at \(host)"
        REGISTRY_AUTH_HTPASSWD_PATH:  "/run/secrets/" + secret_auth
        REGISTRY_HTTP_SECRET:         "VPSGatewayDockerRegistry"
      }
    }
  }
}

stacks: vaultwarden: {
  version: string | *"1.26.0-alpine"

  host: string | *"bw.\(domain)"
  token: string | *""

  smtp: {
    enabled: bool | *false
    host: string
    port: 25 | 465 | 587
    from: string
    user: string
    pass: string
  }

  volume: name: string | *"vaultwarden_data"

  stackfile: {
    let volume_data = "data"
    volumes: (volume_data): {}
    services: "server": {
      image: "vaultwarden/server:" + version
      volumes: [volume_data + ":/data/"]
      environment: {
        DOMAIN: "https://\(host)"
        WEBSOCKET_ENABLED: "true"

        SIGNUPS_ALLOWED: "false"
        INVITATIONS_ALLOWED: "true"
        INVITATION_ORG_NAME: "Vaultwarden at \(host)"
        ORG_CREATION_USERS: "none" // Override in config.json

        if token != "" {
          ADMIN_TOKEN: token
        }

        if smtp.enabled {
          HELO_NAME: host
          SMTP_HOST: smtp.host
          SMTP_PORT: smtp.port
          if smtp.port == 25 {
            SMTP_SECURITY: "off"
          }
          if smtp.port == 465 {
            SMTP_SECURITY: "force_tls"
          }
          if smtp.port == 587 {
            SMTP_SECURITY: "starttls"
          }
          SMTP_FROM: smtp.from
          SMTP_USERNAME: smtp.user
          SMTP_PASSWORD: smtp.pass

          SIGNUPS_VERIFY: "true"
        }
      }
      let name = "vaultwarden"
      #traefik: true
      #labels: [
        "traefik.http.routers.\(name).rule=Host(`\(host)`)",
        "traefik.http.routers.\(name).service=\(name)",
        "traefik.http.services.\(name).loadbalancer.server.port=80",
        "traefik.http.routers.\(name)-ws.rule=Host(`\(host)`) && Path(`/notifications/hub`)",
        "traefik.http.routers.\(name)-ws.service=\(name)-ws",
        "traefik.http.services.\(name)-ws.loadbalancer.server.port=3012",
      ]
      healthcheck: {
        test: ["CMD", "curl", "-f", "http://127.0.0.1"]
        interval: "3s"
        timeout: "3s"
        retries: 3
        start_period: "3s"
      }
    }

  }
}
