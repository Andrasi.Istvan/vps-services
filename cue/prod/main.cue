package main

domain: "4151.hu"
secret_store: "bitwarden"

stacks: traefik: {
  auth: name: "traefik_auth_B"
  acme: {
    enabled: true
    server: "https://acme-v02.api.letsencrypt.org/directory"
    email: "isiandrasi@gmail.com"
  }
  config: {
    name: "traefik_config_B"
    yaml: {
      tls: stores: default: defaultGeneratedCert: {
        "domain": main: domain
        resolver: "letsencrpyt"
      }
    }
  }
}

stacks: registry: {
  auth: name: "registry_auth_A"
}

stacks: vaultwarden: {
  smtp: {
    enabled: true
    host: "smtp.gmail.com"
    port: 587
    user: _
    from: user
  }
}
