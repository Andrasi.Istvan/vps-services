package main

domain: "vps.local"
ssl_dir: "/opt/ssl"

stacks: traefik: {
  auth: {
    user: "user"
    pass: "pass"
  }
  volume: {
    opts: device: ssl_dir
  }
  config: {
    yaml: {
      tls: stores: default: defaultCertificate: {
        certFile: traefik.volume.target + "/\(domain).crt"
        keyFile:  traefik.volume.target + "/\(domain).key"
      }
      // tls: stores: default: defaultGeneratedCert: {
      //   "domain": main: domain
      //   resolver: "letsencrpyt"
      // }
    }
  }
}

stacks: registry: {
  auth: {
    user: "user"
    pass: "pass"
  }
}

// stacks: vaultwarden: token: "token"
