package main

import (
    "tool/cli"
    "tool/exec"
    "encoding/yaml"

    "gitlab.com/andrasi.istvan/cue:bitwarden"
    "gitlab.com/andrasi.istvan/cue:docker"
    "gitlab.com/andrasi.istvan/cue:ssl"
)

secret_store: "bitwarden" | *""

ssl_dir?: string
ssl_var: { "domain": domain, files: ssl.#files & { if ssl_dir != _|_ { dir: ssl_dir } } }

command: "ssl_from_env_files": ssl.from_env_files & { var: ssl_var }
command: "ssl_selfsigned":     ssl.selfsigned     & { var: ssl_var }
command: "ssl_trust":          ssl.trust          & { var: ssl_var }

command: "create_traefik_volume": docker.volume_create & { var: stacks.traefik }
command: "remove_traefik_volume": docker.volume_remove & { var: stacks.traefik }
command: "create_traefik_config": docker.config_create & { var: stacks.traefik }
command: "remove_traefik_config": docker.config_remove & { var: stacks.traefik }
command: "remove_traefik_secret": docker.secret_remove & { var: secret: name: stacks.traefik.auth.name }
command: "create_traefik_secret": auth_secrets & auth_htpasswd & { var: stacks.traefik, create: docker.secret_create & {"var": var} }

command: "remove_registry_secret": docker.secret_remove & { var: secret: name: stacks.registry.auth.name }
command: "create_registry_secret": auth_secrets & auth_htpasswd & { var: stacks.registry, create: docker.secret_create & {"var": var} }

command: "registry_login": auth_secrets & {
  var: stacks.registry
  login: exec.Run & {
    cmd: ["docker", "login", var.host, "-u", var.auth.user, "--password-stdin"]
    stdin: var.auth.pass
  }
}

command: "create_caddy_config": docker.config_create & { var: stacks.php.caddy }
command: "remove_caddy_config": docker.config_remove & { var: stacks.php.caddy }

command: "list_stacks":   cli.Print & {text: yaml.Marshal([for stackname, _     in stacks { stackname }])}
command: "list_services": cli.Print & {text: yaml.Marshal([for stackname, stack in stacks for service, _ in stack.stackfile.services { stackname + "_" + service }])}
command: "list_images":   cli.Print & {text: yaml.Marshal([for _,         stack in stacks for _, service in stack.stackfile.services { service.image }])}

for stackname, stack in stacks for name, service in stack.stackfile.services {
    command: "build_\(stackname)_\(name)": docker.build & { var: service: service }
    command: "push_\(service.image)": docker.push & { var: service }
}

for name, stack in stacks {
    command: "stackfile_\(name)": {
        _stack: stack
        if secret_store != "" {
            _stack: ["auth"]: user: "<username from \(secret_store)>"
            _stack: ["auth"]: pass: "<password from \(secret_store)>"
            _stack: ["smtp"]: user: "<username from \(secret_store)>"
            _stack: ["smtp"]: pass: "<password from \(secret_store)>"
        }
        print: cli.Print & { text: yaml.Marshal(_stack.stackfile) }
    }
    command: "deploy_\(name)": {
        _stack: stack
        deploy: docker.stack_deploy & { var: stackname: name, var: stackfile: _stack.stackfile }
        if secret_store != "" {
            if stack.auth != _|_ {
                auth_secrets: auth_secrets & { var: host: "VPS " + stack.host }
                _stack: auth: auth_secrets.var.auth
            }
            if stack.smtp != _|_ {
                smtp_secrets: auth_secrets & { var: host: "VPS " + stack.host + " " + stack.smtp.host }
                _stack: smtp: smtp_secrets.var.auth
            }
        }
    }
    command: "update_\(name)": docker.stack_stack_update & { var: stackname: name, var: stackfile: stack.stackfile }
    command: "destroy_\(name)": docker.stack_destroy & { var: stackname: name }
}

auth_secrets: {
    var: host: string
    var: auth: {
        user: string
        pass: string
    }
    if secret_store == "" {
        secrets: cli.Print & { text: "Using plaintext auth secrets for host '\(var.host)'!" }
    }
    if secret_store == "bitwarden" {
        secrets: bitwarden.secrets & { "var": name: var.host }
        var: auth: {
            user: secrets.user.stdout
            pass: secrets.pass.stdout
        }
    }
}

auth_htpasswd: {
    var: auth: {
        name: string
        user: string
        pass: string
    }
    var: secret: {
        name: var.auth.name
        data: htpasswd.stdout
    }
    htpasswd: exec.Run & {
        cmd: ["htpasswd", "-nbB", var.auth.user, var.auth.pass]
        stdout: string
    }
}
