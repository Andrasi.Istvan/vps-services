package vps

import "gitlab.com/andrasi.istvan/cue:docker"

domain: =~"^[0-9a-zA-Z-]+(.[0-9a-zA-Z-]+)+$"
stacks: [stackname=docker.#stackname]: stackfile: docker.#stackfile & {
    version: "3.8"
    networks: default: {
        name: "traefik"
        external: true
    }
    services: [_]: {
        init: true
        deploy: restart_policy: {
            condition: "on-failure"
        }
        deploy: update_config: {
            order: "start-first"
            monitor: "3s"
            failure_action: "rollback"
        }

        #traefik: bool | *false
        #traefik_rule: string | *""
        #traefik_port: (>0 & <65536) | *80
        #traefik_tls_main: string | *""
        #traefik_tls_sans: [...string] | *[]

        #labels:              [...string] | *[]
        #labels_traefik:      [...=~"^traefik\\..+="] | *[]
        #labels_traefik_tls:  [...=~"^traefik\\..+="] | *[]
        #labels_traefik_rule: [...=~"^traefik\\..+="] | *[]

        if #traefik {
            #labels_traefik: [
                "traefik.enable=true",
                "traefik.docker.network=traefik",
            ]
        }
        if #traefik_tls_main != "" {
            #labels_traefik_tls: [
                "traefik.http.routers.\(stackname).tls.domains[0].main=\(#traefik_tls_main)",
                for i, san in #traefik_tls_sans {
                    "traefik.http.routers.\(stackname).tls.domains[0].sans[\(i)]=\(san)"
                }
            ]
        }
        if #traefik_rule != "" {
            #labels_traefik_rule: [
                "traefik.http.routers.\(stackname).rule=\(#traefik_rule)",
                "traefik.http.services.\(stackname).loadbalancer.server.port=\(#traefik_port)",
            ]
        }

        deploy: labels: #labels + #labels_traefik + #labels_traefik_tls + #labels_traefik_rule
    }
}
