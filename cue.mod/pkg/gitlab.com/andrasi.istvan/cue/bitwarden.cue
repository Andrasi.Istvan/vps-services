package bitwarden

import "tool/exec"

secrets: {
  var: name: string

  check: exec.Run & { cmd: ["bw", "unlock", "--check"] }

  user: exec.Run & { after: check, cmd: ["bw", "--raw", "get", "username", var.name], stdout: string }
  pass: exec.Run & { after: check, cmd: ["bw", "--raw", "get", "password", var.name], stdout: string }
}
