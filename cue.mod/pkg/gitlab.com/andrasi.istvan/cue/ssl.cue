package ssl

import "tool/os"
import "tool/cli"
import "tool/exec"
import "tool/file"

#files: {
    name: string
    dir: =~"^/" & =~"[^/]$"
    key: dir + "/" + name + ".key"
    crt: dir + "/" + name + ".crt"
}

from_env: {
  var: files: #files
  env: os.Getenv & {
    SSL_KEY: string
    SSL_CRT: string
  }
  w_key: file.Create & { filename: var.files.key, contents: env.SSL_KEY, permissions: 0o600 }
  w_crt: file.Create & { filename: var.files.crt, contents: env.SSL_CRT, permissions: 0o666 }
}

from_env_files: {
  var: files: #files
  env: os.Getenv & {
    SSL_KEY: string
    SSL_CRT: string
  }
  r_key: file.Read   & { filename: env.SSL_KEY }
  r_crt: file.Read   & { filename: env.SSL_CRT }
  w_key: file.Create & { filename: var.files.key, contents: r_key.contents, permissions: 0o600 }
  w_crt: file.Create & { filename: var.files.crt, contents: r_crt.contents, permissions: 0o666 }
}

selfsigned: {
    var: domain: string
    var: files: #files
    var: files: name: string | *var.domain

    config: file.Create & {
        filename: "/tmp/cue-openssl.conf"
        contents: """
        [req]
        prompt             = no
        distinguished_name = req_name
        x509_extensions    = san
        [req_name]
        commonName         = \(var.domain)
        [san]
        subjectAltName     = DNS:\(var.domain),DNS:*.\(var.domain)
        """
    }

    create: exec.Run & {cmd: ["openssl", "req", "-x509", "-nodes", "-newkey", "rsa:4096", "-sha256", "-days", "3650", "-keyout", var.files.key, "-out", var.files.crt, "-config", config.filename]}

    report: after: create
    report: cli.Print & {
        text: """
        Created self-signed certificate and key:
        '\(var.files.crt)'
        '\(var.files.key)'
        """
    }
}

trust: {
    var: domain: string
    var: files: #files
    var: files: name: string | *var.domain

    env: os.Getenv & {
        HOME: string
    }

    add: exec.Run & { cmd: ["certutil", "-d", env.HOME + "/.pki/nssdb", "-A", "-n", var.domain, "-t", "C,,", "-i", var.files.crt] }

    report: cli.Print & {
        text: "Certificate \(var.files.crt) added to NSS store."
        after: add
    }
}
