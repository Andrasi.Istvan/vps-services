package docker

import "list"
import "tool/exec"
import "tool/file"
import "encoding/yaml"

#config: name: string
#config: {data: string | bytes} | {yaml: {...}}

#secret: name: string
#secret: {data: string | bytes} | {file: string}

#volume: {
    name: string
    label: string | *""
    driver: *"local" | "fake"
    opts: [string]: string
    if driver == "local" { opts: #volume_local_opts }
    ["target"]: =~"^/"
}
#volume_local_opts: *{} | {o: =~"(^|,)bind($|,)" | *"bind", device: =~"^/" & =~"[^/]$", type: "none"}

config_create: exec.Run & {
    var: config: #config
    cmd: ["docker", "config", "create", var.config.name, "-"]
    if var.config.data != _|_ { stdin: var.config.data }
    if var.config.yaml != _|_ { stdin: yaml.Marshal(var.config.yaml) }
}

secret_create: exec.Run & {
    var: secret: #secret
    cmd: ["docker", "secret", "create", var.secret.name, "-"]
    if var.secret.data != _|_ { stdin: var.secret.data }
    if var.secret.file != _|_ { stdin: read_secret.contents, read_secret: file.Read & {filename: var.secret.file} }
}

volume_create: exec.Run & {
    var: volume: #volume
    cmd: ["docker", "volume", "create", "--name", var.volume.name, "--driver", var.volume.driver,
        for i in ["--label", var.volume.label] if var.volume.label != "" { i }
    ] + list.FlattenN([for k, v in var.volume.opts {["--opt", k + "=" + v]}], -1)
}

volume_remove: exec.Run & {var: volume: #volume, cmd: ["docker", "volume", "rm", var.volume.name]}
config_remove: exec.Run & {var: config: #config, cmd: ["docker", "config", "rm", var.config.name]}
secret_remove: exec.Run & {var: secret: #secret, cmd: ["docker", "secret", "rm", var.secret.name]}

#stackname: =~"^[0-9a-zA-Z][0-9a-zA-Z-_]+$"
#stackfile: {
    version: *"3.8" | "3.7" | "3.6" | "3.5" | "3.4" | "3.3" | "3.2" | "3.1" | "3.0" | "2.4" | "2.3" | "2.2" | "2.1" | "2.0"
    services:     [string]: #service
    ["volumes"]:  [string]: {["name"]: string, ["external"]: bool | *false, ...}
    ["configs"]:  [string]: {["name"]: string, ["external"]: bool | *false, ...}
    ["secrets"]:  [string]: {["name"]: string, ["external"]: bool | *false, ...}
    ["networks"]: [string]: {["name"]: string, ["external"]: bool | *false, ...}
}

#service: {
    image: string
    ["build"]: {
        context: string | *""
        dockerfile: string
    }
    ["command"]:     string | [...string]
    ["volumes"]:     [...(string | {...})]
    ["configs"]:     [...(string | {...})]
    ["secrets"]:     [...(string | {...})]
    ["networks"]:    [...(string | {...})]
    ["environment"]: [string]: string | int
    ...
}

build: exec.Run & {
    var: service: #service
    cmd: ["docker", "build", "-t", var.service.image, "-f", "-", if var.service.build.context != "" { var.service.build.context }]
    stdin: var.service.build.dockerfile
}

push: exec.Run & { var: image: string, cmd: ["docker", "push", var.image] }

stack_deploy: exec.Run & {
    var: stackname: #stackname
    var: stackfile: #stackfile
    cmd: ["docker", "stack", "deploy", "--with-registry-auth", "-c", "-", var.stackname]
    stdin: yaml.Marshal(var.stackfile)
}

stack_destroy: exec.Run & {
    var: stackname: #stackname
    docker_stack_destroy: exec.Run & {
        cmd: ["docker", "stack", "rm", var.stackname]
    }
}

stack_update: {
    var: stackname: #stackname
    var: stackfile: #stackfile
    for name in var.stackfile.services { (name): service_update & {service: var.stackname + "_" + name} }
}

stack_restart: {
    var: stackname: #stackname
    var: stackfile: #stackfile
    for name in var.stackfile.services { (name): service_restart & {service: var.stackname + "_" + name} }
}

service_update: exec.Run & {
    var: service: string
    cmd: ["docker", "service", "update", var.service]
}

service_restart: {
    var: service: string
    var: replicas: >0 | *1
    scale_d: exec.Run & { cmd: ["docker", "service", "scale", var.service + "=0"] }
    scale_u: exec.Run & { cmd: ["docker", "service", "scale", var.service + "=\(var.replicas)"], after: scale_d }
}
