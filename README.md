VPS gateway and services with dynamic routing using `Traefik` and `Docker`.

## Overview
- `Traefik` accepts `HTTP` connections on port `80` and redirects them to `HTTPS` on port `443`, accepts `TLS/HTTPS` connections on port `443`
- `Docker` provides a deployment target for backend services with dynamic routing through `Traefik`

~~~plantuml
@startuml

skinparam DefaultFontSize 11
skinparam DefaultFontName Go Mono, monospace
skinparam RoundCorner 15
skinparam Shadowing false
skinparam Cloud {
  BackgroundColor transparent
  BorderColor     #000
}
skinparam Interface {
  BackgroundColor transparent
  BorderColor     #000
}

() " " as WWW

rectangle Traefik {
  () "\n:80\n(HTTP)" as Traefik.HTTP
  () "\n:443\n(HTTPS)" as Traefik.HTTPS
}

cloud "Docker services\nDynamic routing" as Docker #line.dotted

WWW -[#A00]-> Traefik.HTTP
WWW -[#0A0]-> Traefik.HTTPS
Traefik.HTTP  -[#AAA]> Traefik.HTTPS #line.dotted
Traefik.HTTPS -[#A00]> Docker
Traefik.HTTPS -[#A0A]  Docker

@enduml
~~~

## Usage
Edit [`cue/default/main.cue`](cue/default/main.cue) or create a new configuration:
~~~
export CUE_ENV="myenv"
mkdir "cue/$CUE_ENV"
cp cue/default/main.cue "cue/$CUE_ENV/main.cue"
$EDITOR "cue/$CUE_ENV/main.cue"
~~~

List available stacks and their services:
~~~
bin/run list_stacks
bin/run list_services
~~~

Check the resulting compose file for a stack:
~~~
bin/run stackfile_traefik
~~~

# Traefik
Create default certificate for `Traefik`:
~~~
# By generating a self signed certificate:
bin/run ssl_selfsigned

# By using existing crt and key files:
export SSL_KEY=/tmp/keyfile.key
export SSL_CRT=/tmp/crtfile.crt
bin/run ssl_from_env_files

# By using crt and key file contents stored in the environment:
export SSL_KEY=-----BEGIN PRIVATE KEY-----...
export SSL_CRT=-----BEGIN CERTIFICATE-----...
bin/run ssl_from_env

# Optionally add the certificate to the local trust store:
bin/run trust
~~~

Initialize a `Docker` swarm, create the swarm scoped overlay network `traefik` and start the `traefik` stack:
~~~
docker swarm init
docker network create traefik --driver overlay --scope swarm --attachable
bin/run create_traefik_volume
bin/run create_traefik_config
bin/run create_traefik_secret
bin/run deploy_traefik
~~~

Deploy and destroy the `whoami` stack to test routing:
~~~
bin/run deploy_whoami
# Wait a few seconds for automatic service discovery
curl -ki "https://whoami.$(bin/get -e domain --out text)/"
bin/run destroy_whoami
~~~

# Docker registry
~~~
bin/run create_registry_secret
bin/run deploy_registry
bin/run registry_login
~~~
